# WGT

## What is?
WGT is a desktop app for displaying wigits.

## How to run?
To run WGT you need to have [tauri](https://tauri.studio/en/) set up for your machine.  After that running `npm run tauri dev` will start up the dev environment.