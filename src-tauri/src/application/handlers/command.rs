use crate::application::dto::payload::{Payload};
use crate::application::errors::command_process_error::{ CommandProcessError };
use crate::application::traits::command::{Command};
use crate::domain::entity::wigit::wigit::{ Wigit };
pub struct CommandHandler<'a> {
    commandProcess: &'a dyn Command
}

impl<'a> CommandHandler<'a> {
    pub fn of(commandProcess: &'a dyn Command) -> Self{
        return Self {
            commandProcess
        }
    }
    fn invoke(&self, wigit: Wigit ) -> Payload<String>{
        let output = self.commandProcess
            .invoke(String::from(wigit.run()), wigit.args().clone());

        match output {
            Ok(o) => Payload { message: o.into() },
            Err(e) => 
                match e {
                    CommandProcessError => Payload { message: "There was an error processing the command".into() }
                }
        }
    }
}