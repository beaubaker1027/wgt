use std::fmt;
use std::error;

#[derive(Debug, Clone)]
pub struct CommandProcessError {
    message: String
}

impl fmt::Display for CommandProcessError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        return write!(
            f, 
            "CommandProcessError {{ message: {} }}", 
            self.message
        );
    }
}

impl error::Error for CommandProcessError {}