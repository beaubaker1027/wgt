use crate::domain::entity::wigit::alias::{ Arg };
use crate::application::errors::command_process_error::{ CommandProcessError };

pub trait Command {
    fn invoke(&mut self, command: String, arg: Option<Vec<Arg>>) -> Result<String, CommandProcessError>;
}