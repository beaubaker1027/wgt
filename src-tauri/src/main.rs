#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

mod lib;
mod domain;
mod application;
mod infrastructure;

// main
fn main() {
  tauri::Builder::default()
  .setup(|_app| {
    Ok(())
  })
  .on_page_load(|win, _payload| {
      let default_config_location = ".wgt/config.json";
      match win.label() {
        "main" => {
          let emitter = win.clone();
          win.once("config", move |_event| {
            let home = tauri::api::path::home_dir();
            match home {
              Some(mut path) => {
                path.push(default_config_location);
                let config = from_json(
                  tauri::api::file::read_string(path).unwrap()
                );
                println!("{:?}", config);
                emitter.emit("config", Payload { message: config }).unwrap();
              },
              None => {

              }
            }
          });
        },
        _ => {

        }
      }
    })
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
