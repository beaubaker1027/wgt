use derive_getters::Getters;
use serde::{Serialize, Deserialize};
use crate::domain::entity::wigit::alias::{ Label, Run, Arg, Output };

#[derive(Getters, Serialize, Deserialize, Debug)]
pub struct Wigit {
  label: Label,
  run: Run,
  args: Option<Vec<Arg>>,
  output: Option<Output>,
}

impl Wigit {
  fn of(
    label: String,
    run: String,
    args: Option<Vec<Arg>>,
    output: Option<String> ) -> Wigit {
    return Wigit {
      label,
      run,
      args,
      output
    }
  }
}