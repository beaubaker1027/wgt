pub type Color = String;
pub type Json = String;
pub type Path = String;
pub type Size = i16;