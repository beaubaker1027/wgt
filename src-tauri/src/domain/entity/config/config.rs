use serde::{Serialize, Deserialize};
use serde_json::{from_str};

use crate::domain::entity::wigit::wigit::{Wigit};
use crate::domain::entity::config::alias::{Color, Path, Size, Json};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
  pub theme: Theme,
  pub apps: Vec<Wigit>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Theme {
  pub colors: Colors,
  pub font: Font
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Colors {
  pub background: Option<Color>,
  pub foreground: Option<Color>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Font {
  pub path: Option<Path>,
  pub size: Option<Size>
}

pub fn from_json(json: Json) -> Config {
  return from_str::<Config>(&json).unwrap();
}
