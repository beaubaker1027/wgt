use std::str::{ from_utf8 };
use std::process::Command;
use crate::domain::entity::wigit::alias::{ Arg };
use crate::application::errors::command_process_error::{CommandProcessError};
use crate::application::traits::command::{ Command as TCommand };

pub struct RustProcess {
    command: Command,
}

type Result<T> = std::result::Result<T, CommandProcessError>;

impl RustProcess {
    pub fn of(command: Command) -> RustProcess {
        return RustProcess {
            command
        }
    }
}

impl TCommand for RustProcess {
    fn invoke (&mut self, command:String, args:Vec<Option<Arg>>) -> Result<String> {
        let args_:Vec<Arg> = args
                    .into_iter()
                    .flatten()
                    .collect();

        let output = self.command.arg(command)
                    .args(args_)
                    .output()
                    .map(|o| {
                        from_utf8(&o.stdout)
                        .map(|o| o.to_string())
                    })
                    .map_err(|_e|
                       CommandProcessError { message: "Command  failed to execute".into() }
                    );

        match output {
            Ok(output) => 
                output
                .map_err(|_e| 
                    CommandProcessError { message: "Command process failed to return a string".into() } 
                ),
            Err(e) => Err(e)
        }
    }
}

