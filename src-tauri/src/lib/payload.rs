use serde::{Serialize};

#[derive(Serialize)]
pub struct Payload<T> {
  pub message: T
}