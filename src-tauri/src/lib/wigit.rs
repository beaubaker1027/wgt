use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Wigit {
  pub label: String,
  pub run: String,
  pub args: Option<Vec<Option<Arg>>>,
  pub output: Option<String>,
}

type Arg = String;