import React from 'react'
import { once, emit } from '@tauri-apps/api/event';
import styled, { ThemeProvider } from 'styled-components';
import './App.css'

interface Config {
  theme: Theme;
  apps: Apps
};

interface Theme {
  colors: {
    background: string;
    foreground: string;
  },
  font: {
    path: string;
    size: number;
  }
}

interface App {
  label: string;
  run: string;
}
  
type Apps = App[];

const mergeThemes = 
  ( dTheme: Theme ) => 
  (theme: Partial<Theme> ):Theme => ( 
    { colors: 
        { background: theme?.colors?.background || dTheme.colors.background
        , foreground: theme?.colors?.foreground || dTheme.colors.foreground }
    , font:
        { path: theme?.font?.path || dTheme.font.path
        , size: theme?.font?.size || dTheme.font.size } }
);

const defaultTheme:Theme = {
  colors: {
    background: '#000000',
    foreground: '#f2f2f2'
  },
  font: {
    path: "",
    size: 16
  }
};

const Div = styled.div`
  background-color: ${props => props.theme.colors.background};
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const Header = styled.h1`
  color: ${props => props.theme.colors.foreground};
  font-size: ${props => props.theme.font.size}px;
  font-family: monospace;
`

function App() {
  const [ theme, setTheme ] = React.useState<Theme>(defaultTheme);
  const [ apps, setApps ] = React.useState<Apps>([]);

  console.log('apps', apps);
  React.useEffect(() => {
    const listener = once<{message: Partial<Config>}>(
      'config',
      ({payload}) => {
        console.log('payload', !!payload);
        if (!!payload) {
          setApps(payload.message.apps ?? []);
          setTheme(mergeThemes(defaultTheme)(payload.message.theme ?? {})); 
        };
      }
    );
    emit('config');
    return () => {
      listener.then(unlistenFn => unlistenFn());
    };
  }, [ setTheme, setApps ]);

  return (
    <ThemeProvider theme={theme}>
      <Div className="App">
        <Header style={{color:'#ffffff'}}>{apps.length}</Header>
        <ul>
        {
          apps.map((app, i) =>
            <li key={i}>
              <h1 style={{color: "#ffffff", fontFamily: 'monospace'}}>{app.run}</h1>
            </li>
          )
        }
        </ul>
      </Div>
    </ThemeProvider>
  )
}

export default App
